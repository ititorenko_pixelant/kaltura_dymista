<?php
/**
 * VideoBox controller.
 *
 * @author Jozef Spisiak <jozef@pixelant.se>
 * @package Kaltura
 * @subpackage Controller
 * @version $Id:$
 */

class Tx_Kaltura_Controller_VideoBoxController extends Tx_Extbase_MVC_Controller_ActionController {


	protected function overrideSettingsFromTS(){
		if($this->settings['overrideSettings']){
			$overrideSettings = t3lib_div::trimExplode(',', $this->settings['overrideSettings']);
			$typoScriptService = $this->objectManager->get('Tx_Extbase_Service_TypoScriptService'); 
			$settingsAsTypoScriptArray = $typoScriptService->convertPlainArrayToTypoScriptArray($this->settings);
			foreach($overrideSettings as $key){
				$this->settings[$key] = $this->configurationManager->getContentObject()->stdWrap($settingsAsTypoScriptArray[$key], $settingsAsTypoScriptArray[$key.'.']);
				$settingsChanged = TRUE;
			}
			$this->view->assign('settings', $this->settings);	
		}
	}

	/**
	 * Render box
	 *
	 * @return string
	 */
	public function indexAction() {
		$this->overrideSettingsFromTS();

		try {
		$conf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['kaltura']);

		if ($conf['partnerID'] == 0 || $conf['adminSecret'] == '' || $conf['userID'] == '')
		{
			$this->view->assign('error', Tx_Extbase_Utility_Localization::translate('bad_configuration', 'kaltura'));
			return $this->view->render();
		}

		if ($this->settings['type'] == 1) $this->view->assign('playlist',1);


		$cObj = $this->configurationManager->getContentObject();
		$data = $cObj->data;

		$this->view->assign('uid', $data['uid']);
		$this->view->assign('cacheTime', $data['tstamp']);

		$this->view->assign('partnerID', $conf['partnerID']);
		$this->view->assign('subPartnerID', $conf['subPartnerID']);
		$this->view->assign('playerUI', $this->settings['skin']);
		
		$configuration = t3lib_div::makeInstance('KalturaConfiguration', $conf['partnerID']);
		$host = ($conf['kalturaHost'] != '')?$conf['kalturaHost'] : 'www.kaltura.com';
		$configuration->serviceUrl = 'http://'.$host.'/';
		$this->view->assign('kalturaUrl', $host);
		/**
		 * @var KalturaClient
		 */
		$client = t3lib_div::makeInstance('KalturaClient', $configuration);
		$ks = $client->session->start($conf['adminSecret'], $conf['userID'], KalturaSessionType::ADMIN);
		$client->setKs($ks);
		
		$ui = $client->uiConf->get($this->settings['skin']);
		$this->view->assign('height', floor((intval($this->settings['width']) * (intval($ui->height)-32)) / intval($ui->width)) + 32);
		
		if ($this->settings['type'] == 0)
		{
			$entry = $client->media->get($this->settings['kalturaContent']);
	
			if ($this->checkEntryStatus($entry->status))
				$this->view->assign("entry", $entry);
		} else {
			$entry = $client->playlist->get($this->settings['kalturaPlaylist']);
			$this->view->assign("entry", $entry);
		}
		} catch (Exception $e) {
			$this->view->assign("error",Tx_Extbase_Utility_Localization::translate('problem_with_video', 'kaltura'));
		}
	}

	private function checkEntryStatus($status) {
		switch ($status)
		{
			case KalturaEntryStatus::READY :
				$return = TRUE;
				break;
			case KalturaEntryStatus::BLOCKED :
				$message = Tx_Extbase_Utility_Localization::translate('status_blocked', 'kaltura');
			case KalturaEntryStatus::DELETED :
				$message = Tx_Extbase_Utility_Localization::translate('status_deleted', 'kaltura');
			case KalturaEntryStatus::ERROR_CONVERTING :
				$message = Tx_Extbase_Utility_Localization::translate('status_error_converting', 'kaltura');
			case KalturaEntryStatus::ERROR_IMPORTING :
				$message = Tx_Extbase_Utility_Localization::translate('status_error_importing', 'kaltura');
			case KalturaEntryStatus::IMPORT :
				$message = Tx_Extbase_Utility_Localization::translate('status_import', 'kaltura');
			case KalturaEntryStatus::INFECTED :
				$message = Tx_Extbase_Utility_Localization::translate('status_infected', 'kaltura');
			case KalturaEntryStatus::MODERATE :
				$message = Tx_Extbase_Utility_Localization::translate('status_moderate', 'kaltura');
			case KalturaEntryStatus::NO_CONTENT :
				$message = Tx_Extbase_Utility_Localization::translate('status_no_content', 'kaltura');
			case KalturaEntryStatus::PENDING :
				$message = Tx_Extbase_Utility_Localization::translate('status_pending', 'kaltura');
			case KalturaEntryStatus::PRECONVERT :
				$message = Tx_Extbase_Utility_Localization::translate('status_preconvert', 'kaltura');
			default :
				$return = FALSE;
				if (!isset($message))
					$message = Tx_Extbase_Utility_Localization::translate('status_unknown', 'kaltura');
				break;
		}
		if (isset($message))
			$this->view->assign('error', $message);
		return $return;
	}

	/**
	 * Function to list video players
	 *
	 * @return array
	 */
	function user_listPlayersAction($config) {
		$conf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['kaltura']);

		if ($conf['partnerID'] == 0 || $conf['adminSecret'] == '' || $conf['userID'] == '')
		{
			$this->view->assign('error', Tx_Extbase_Utility_Localization::translate('bad_configuration', 'kaltura'));
			return $this->view->render();
		}

		try {
			$configuration = t3lib_div::makeInstance('KalturaConfiguration', $conf['partnerID']);
			$host = ($conf['kalturaHost'] != '')?$conf['kalturaHost'] : 'www.kaltura.com';
			$configuration->serviceUrl = 'http://'.$host.'/';
			/**
			 * @var KalturaClient
			 */
			$client = t3lib_div::makeInstance('KalturaClient', $configuration);
			$ks = $client->session->start($conf['adminSecret'], $conf['userID'], KalturaSessionType::ADMIN);
			$client->setKs($ks);

			$playerss = $client->uiConf->listAction();
			$add = array();
			foreach ($playerss as $players) 
				if (is_array($players))
					foreach ($players as $player)
						$add[] = array((string)$player->name,intval($player->id));

			$config['items'] = array_merge($config['items'],$add);

		} catch (KalturaException $e) {
			$config['items'] = array_merge($config['items'],array(array('Could not connect to kaltura host with provided credentials','0')));
		}
		return $config;
	}

	/**
	 * Render the field for the VideosTest
	 *
	 * Generates html with an standard flexform input, but set value to the input with javascript to be able to display the select video more graphical.
	 *
	 * @param unknown_type $PA
	 * @param unknown_type $fobj
	 */
	public function user_listVideosSelectAction($PA, $fobj) {
		// return '<p>Check</p>';
		// Fetch PageRenderer so we can add js and css to output
		$pageRenderer = t3lib_div::makeInstance('t3lib_PageRenderer');

		// Build paths for js and css
		$pathJs = '../' . t3lib_extMgm::siteRelPath('kaltura').'Resources/Public/Js/';
		$pathCss = '../' . t3lib_extMgm::siteRelPath('kaltura').'Resources/Public/Css/';

		// Add js and css to pagerenderer
    	$pageRenderer->addJsFile($pathJs . 'jquery-1.8.2.min.js', 'text/javascript', true);
    	$pageRenderer->addJsFile($pathJs . 'kaltura_be_func.js', 'text/javascript', true);
    	$pageRenderer->addCssFile($pathCss . 'kaltura_be.css','stylesheet','all');

    	// Get data from Kaltura
		$videoData = $this->buildSelectVideoData();

		// Add input element where to store the video id (TODO: hidden)
		$inputStoreVideoId = '<input type="hidden" id="selected_video" name="'.$PA['itemFormElName'].'" value="'.htmlspecialchars($PA['itemFormElValue']).'" />';
		// Build output html
		$htmlContent = '<div class="kaltura-video-select">';

		// Build category-list
		$htmlContent.= '<div class="select-category-list">';
		$htmlContent.= '<h3>Category</h3>';
		$htmlContent.= $this->generateCategoryListHtml(0,$videoData['categories'], $this->getActiveCategoryId($videoData['videos'],$PA['itemFormElValue']));
		$htmlContent.= '</div>';

		// Build video-list
		$htmlContent.= '<div class="select-video-list">';
		$htmlContent.= '<h3>Video</h3>';
		$htmlContent.= $this->generateVideoListHtml($videoData['videos'], $PA['itemFormElValue']);
		$htmlContent.= '</div>';

		// Build video "single-views"
		$htmlContent.= '<div class="view-video-item">';
		$htmlContent.= '<h3>More info</h3>';
		$htmlContent.= $this->generateVideoViewsHtml($videoData['videos'], $PA['itemFormElValue']);
		$htmlContent.= $inputStoreVideoId;
		$htmlContent.= '</div>';

		// Clear
		$htmlContent.= '<div style="clear:both;"></div></div>';

		// Return html
		return $htmlContent;

	}

	/**
	 * Generate the video list html - ul li
	 *
	 * @param array $videos
	 * @param string $selectedVideoId
	 */
	private function generateVideoListHtml($videos, $selectedVideoId) {
		$returnHtml = '<ul class="video-list">';

        $videos = $this->sortMultiArray($videos, 'name');

		foreach ($videos as $video) {
			if ($video->id == $selectedVideoId) {
                if (!(($video->categories) == NULL) && !(empty($video->categories))) {
                    $returnHtml.= '<li class="active select-video ' . str_replace(".", "_", str_replace(">", " ", str_replace(",", " ", str_replace(" ", "-", $video->categories)))) . '" id="' . $video->id . '">' . $video->name . '</li>';
                } else {
                    $video->categories = 'No category';
                    $returnHtml.= '<li class="select-video ' . str_replace(".", "_", str_replace(">", " ", str_replace(",", " ", str_replace(" ", "-", $video->categories)))) . '" id="' . $video->id . '">' . $video->name . '</li>';
                }

            } else {
                if (!(($video->categories) == NULL) && !(empty($video->categories))) {
                    $returnHtml.= '<li class="select-video ' . str_replace(".", "_", str_replace(">", " ", str_replace(",", " ", str_replace(" ", "-", $video->categories)))) . '" id="' . $video->id . '">' . $video->name . '</li>';
                } else {
                    $video->categories = 'No category';
                    $returnHtml.= '<li class="select-video ' . str_replace(".", "_", str_replace(">", " ", str_replace(",", " ", str_replace(" ", "-", $video->categories)))) . '" id="' . $video->id . '">' . $video->name . '</li>';
                }
            }
		}
		$returnHtml.= '</ul>';
		return $returnHtml;
	}

    /**
     * Sort multi array by value of selected key
     */

    private function sortMultiArray ($mArray, $orderby) {
        $sortArray = array();

        foreach($mArray as $selectedKey){
            foreach($selectedKey as $key=>$value){
                if(!isset($sortArray[$key])){
                    $sortArray[$key] = array();
                }
                $sortArray[$key][] = strtolower($value);
            }
        }

        array_multisort($sortArray[$orderby],SORT_ASC, $mArray);

        return $mArray;
    }


    /**
	 * Generate the video single views html - ul li, only display active video
	 *
	 * @param array $videos
	 * @param string $selectedVideoId
	 */
	private function generateVideoViewsHtml($videos, $selectedVideoId) {
		$returnHtml = '<ul class="video-views">';
		foreach ($videos as $video) {
			if ($video->id == $selectedVideoId) {
				$returnHtml.= '<li class="active view-video" id="view-' . $video->id . '">';
			} else {
				$returnHtml.= '<li class="view-video" id="view-' . $video->id . '" style="display:none;">';
			}
			$returnHtml.= '<div class="video-info">';
			$returnHtml.= '<p class="video-info-name">' . $video->name . '</p>';
			$returnHtml.= '<p class="video-info-image"><img src="' . $video->thumbnailUrl .'" /></p>';
			$returnHtml.= '<p class="video-info-description">' . $video->description . '</p>';
			$returnHtml.= '<table width="100%">';
			$returnHtml.= '<tr><td class="label">Tags:</td><td>' . $video->tags . '</td></tr>';
			$returnHtml.= '<tr><td class="label">Format:</td><td>' . $video->width . ' x ' . $video->height . ' px</td></tr>';
			$returnHtml.= '<tr><td class="label">Duration:</td><td>' . $video->duration . ' seconds</td></tr>';

			$returnHtml.= '</table>';

			$returnHtml.= '</div>';
			$returnHtml.= '</li>';
		}
		$returnHtml.= '</ul>';
		return $returnHtml;
	}

	/**
	 * Generate the category list html - ul li
	 *
	 * @param int $parentId
	 * @param array $categories
	 * @param int $selectedCategoryId
	 */
	private function generateCategoryListHtml($parentId, $categories, $selectedCategoryId) {
		$returnHtml = '<ul class="category-list-' . $parentId . '">';

        $newCategory = new KalturaCategory();
        $newCategory->id = '0';
        $newCategory->fullName = 'No category';
        $newCategory->name = 'No category';

        $categories = $this->sortMultiArray($categories, 'name'); //sorting categories by name
        array_unshift($categories, $newCategory); //set 'No Category' on the first place

        foreach ($categories as $category) {
			if ($category->parentId == $parentId) {
				if ($category->id == $selectedCategoryId) {
                    $returnHtml.= '<li class="active select-category" id="' . $category->id . '" name="' . str_replace(".", "_", str_replace(",", " ", str_replace(" ", "-", $category->fullName))) . '">' . $category->name . '</li>';
				} else {
                    $returnHtml.= '<li class="select-category" id="' . $category->id . '" name="' . str_replace(".", "_", str_replace(",", " ", str_replace(" ", "-", $category->fullName))) . '">' . $category->name . '</li>';
				}
				if (count($category->childNodes) > 0) {
					$returnHtml.= $this->generateCategoryListHtml($category->id, $category->childNodes, $selectedCategoryId);
				}
			}
		}
		$returnHtml.= '</ul>';
		return $returnHtml;
	}

	/**
	 * Goes thru array and check the id of the selected video
	 *
	 * @param array $videos
	 * @param string $selectedVideoId
	 * @return int
	 */
	private function getActiveCategoryId($videos, $selectedVideoId) {
		foreach ($videos as $video) {
			if ($video->id == $selectedVideoId) {
				return $video->categoriesIds;
			}
		}
		return false;
	}

	/**
	 * Function to build arrays to use in new layout of select video
	 *
	 * @return array
	 */
	private function buildSelectVideoData() {

		$returnData = false;

		$conf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['kaltura']);

		if ($conf['partnerID'] == 0 || $conf['adminSecret'] == '' || $conf['userID'] == '')
		{
			echo Tx_Extbase_Utility_Localization::translate('bad_configuration', 'kaltura');
			die();
		}

		try {
			$configuration = t3lib_div::makeInstance('KalturaConfiguration', $conf['partnerID']);
			$host = ($conf['kalturaHost'] != '')?$conf['kalturaHost'] : 'www.kaltura.com';
			$configuration->serviceUrl = 'http://'.$host.'/';
			/**
			 * @var KalturaClient
			 */
			$client = t3lib_div::makeInstance('KalturaClient', $configuration);
			$ks = $client->session->start($conf['adminSecret'], $conf['userID'], KalturaSessionType::ADMIN);
			$client->setKs($ks);

			/**
			 * @var KalturaMediaEntryFilter
			 * Load videos
			 */
			$filter = t3lib_div::makeInstance('KalturaMediaEntryFilter');
			$filter->statusEqual = KalturaEntryStatus::READY;
			$filter->mediaTypeEqual = KalturaMediaType::VIDEO;
			$filter2 = t3lib_div::makeInstance('KalturaFilterPager');
			$filter2->pageSize = 0;
			$mediass = $client->media->listAction($filter,$filter2);
			$add = array();
			foreach ($mediass as $medias) {
				if (is_array($medias)) {
					foreach ($medias as $entry) {
						$add[] = $entry;
					}
				}
			}
			$returnData['videos'] = $add;

			$categoriess = $client->category->listAction();
			$add = array();
			foreach ($categoriess as $category) {
				if (is_array($category)) {
					foreach ($category as $entry) {
						$add[] = $entry;
					}
				}
			}
			$returnData['categories'] = $this->getChildCategories(0,$add);


		} catch (KalturaException $e) {
			$returnData['exception'] = 'Could not connect to kaltura host with provided credentials';
		}
		return $returnData;
	}

	/**
	 * Generate the category hierarchicaly
	 *
	 * @param array $parenId
	 * @param string $categories
	 * @return array
	 */
	private function getChildCategories($parentId, $categories) {
		$childCategories = array();
		foreach ($categories as $category) {
			if ($category->parentId == $parentId) {
				$childCategories[$category->id] = $category;
				$childCategories[$category->id]->childNodes = $this->getChildCategories($category->id, $categories);
			}
		}
		return $childCategories;
	}

	/**
	 * Function to list videos
	 *
	 * @return array (player key => title)
	 */
	function user_listVideosAction($config) {
		$conf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['kaltura']);

		if ($conf['partnerID'] == 0 || $conf['adminSecret'] == '' || $conf['userID'] == '')
		{
			echo Tx_Extbase_Utility_Localization::translate('bad_configuration', 'kaltura');
			die();
		}

		try {
			$configuration = t3lib_div::makeInstance('KalturaConfiguration', $conf['partnerID']);
			$host = ($conf['kalturaHost'] != '')?$conf['kalturaHost'] : 'www.kaltura.com';
			$configuration->serviceUrl = 'http://'.$host.'/';
			/**
			 * @var KalturaClient
			 */
			$client = t3lib_div::makeInstance('KalturaClient', $configuration);
			$ks = $client->session->start($conf['adminSecret'], $conf['userID'], KalturaSessionType::ADMIN);
			$client->setKs($ks);

			/**
			 * @var KalturaMediaEntryFilter
			 */
			$filter = t3lib_div::makeInstance('KalturaMediaEntryFilter');
			$filter->statusEqual = KalturaEntryStatus::READY;
			$filter->mediaTypeEqual = KalturaMediaType::VIDEO;
			$filter2 = t3lib_div::makeInstance('KalturaFilterPager');
			$filter2->pageSize = 300;
			$mediass = $client->media->listAction($filter,$filter2);
			$add = array();
			foreach ($mediass as $medias) 
				if (is_array($medias))
					foreach ($medias as $entry)
						$add[] = array((string)$entry->name,(string)$entry->id);
			$config['items'] = array_merge($config['items'],$add);
		} catch (KalturaException $e) {
			$config['items'] = array_merge($config['items'],array(array('Could not connect to kaltura host with provided credentials','0')));
		}
		return $config;
	}

	/**
	 * Function to list playlists
	 *
	 * @return array (player key => title)
	 */
	function user_listPlaylistsAction($config) {
		$conf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['kaltura']);

		if ($conf['partnerID'] == 0 || $conf['adminSecret'] == '' || $conf['userID'] == '')
		{
			echo Tx_Extbase_Utility_Localization::translate('bad_configuration', 'kaltura');
			die();
		}

		try {
			$configuration = t3lib_div::makeInstance('KalturaConfiguration', $conf['partnerID']);
			$host = ($conf['kalturaHost'] != '')?$conf['kalturaHost'] : 'www.kaltura.com';
			$configuration->serviceUrl = 'http://'.$host.'/';
			/**
			 * @var KalturaClient
			 */
			$client = t3lib_div::makeInstance('KalturaClient', $configuration);
			$ks = $client->session->start($conf['adminSecret'], $conf['userID'], KalturaSessionType::ADMIN);
			$client->setKs($ks);

			$playlistss = $client->playlist->listAction();
			$add = array();
			foreach ($playlistss as $playlists) 
				if (is_array($playlists))
					foreach ($playlists as $playlist)
						$add[] = array((string)$playlist->name,(string)$playlist->id);

			$config['items'] = array_merge($config['items'],$add);
		} catch (KalturaException $e) {
			$config['items'] = array_merge($config['items'],array(array('Could not connect to kaltura host with provided credentials','0')));
		}
		return $config;
	}
}
?>
