jQuery.noConflict();
jQuery(document).ready(function() {
	// Bind click event when selecting video
	jQuery('.select-video').click(function(event) {
		// Fetch selected video id and set id to hidden input
		selectedVideoId = jQuery(this).attr("id");	
		jQuery('#selected_video').val(selectedVideoId);
		// Remove and set active class
		jQuery('.select-video.active').removeClass('active');
		jQuery(this).addClass('active');
		// Hide current video, remove class, show new video and add class
		var activeVideoCount = jQuery('.view-video.active').length;
		if (activeVideoCount > 0) {
			jQuery('.view-video.active').fadeOut('fast', function() {
				jQuery('.view-video.active').removeClass('active');
				jQuery('#view-' + selectedVideoId).fadeIn('fast');
				jQuery('#view-' + selectedVideoId).addClass('active');
			});
		} else {
			jQuery('#view-' + selectedVideoId).fadeIn('fast');
			jQuery('#view-' + selectedVideoId).addClass('active');
		}
	});
	// Bind click event when selecting category
	jQuery('.select-category').click(function(event) {
		// Remove and add active class
		jQuery('.select-category').removeClass('active');
		jQuery(this).addClass('active');
		// Hide all videos in list
		jQuery('.select-video').hide();
		// Get which videos to be visible by getting name from selected category and split it to array
		var categories = jQuery(this).attr('name').split('\>');
		// Loop thru "categories" and check which videos who has this classname,if they do display the video in list
	    jQuery.each(categories, function() { // iterate over array 
	        jQuery('.select-video.' + this).fadeIn('fast');
	    });
	});
	// Click on active category to filer videos
	jQuery('.select-category.active').click();

    //Hide empty categories

    jQuery('.category-list-0 li').each(function(){
        var that = jQuery(this);
        var thatAttr = this.getAttribute("name");

        if ( !(jQuery('.video-list li').hasClass(thatAttr)) ) {
            that.hide();
        }
    });

    jQuery('.video-list').scrollTop(jQuery('.video-list .active').position().top - jQuery('.video-list').position().top -106);
    jQuery('.category-list-0').scrollTop(jQuery('.category-list-0 .active').position().top - jQuery('.category-list-0').position().top -106);

});