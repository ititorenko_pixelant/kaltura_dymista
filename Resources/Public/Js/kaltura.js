// Kaltura video ready callback
function jsCallbackReady(objectId) {
	if(typeof jQuery !== 'undefined' && typeof objectId !== 'undefined'){
		jQuery(document).trigger('Kaltura_jsCallbackReady', [objectId]);
	}
}
